# Fabric Example Mod - Kotlin

## Setup

0. Create a new mod repository by pressing the "Use this template" 
  button and clone the created repository.

1. Edit build.gradle and mod.json to suit your needs.
    * The "mixins" object can be removed from mod.json if you do not need to 
    use mixins.
    * Please replace all occurences of "modid" with your own mod ID - 
    sometimes, a different string may also suffice.
2. Run the following command:

```
./gradlew idea
```
## Useful Links
https://fabricmc.net/wiki/tutorial:setup

## License

This template is available under the CC0 license. Feel free to learn from it 
and incorporate it in your own projects.

## Tutorials

### Creating and registering a new item
First several tools must be imported:
  *	import net.fabricmc.api.ModInitializer;
  *	import net.minecraft.item.Items;
  *	import net.minecraft.item.ItemGroup;
  *	import net.minecraft.item.Item;
  *	import net.minecraft.util.Identifier;
  *	import net.minecraft.util.registry.Registry;

Mod initializer is used to tell minecraft to load all things here on startup.
all net.minecraft imports are related to Classes from minecraft that we'll be
using and adjusting. In the test item demonstration we added and registered an
item so the Item related Classes are imported as well as the Identifier 
(used to provide identification for items and blocks) as well as the registry
itself.

#### To create a new item
  val testItem = Item(Item.Settings().group(ItemGroup.MISC))

  Notes:
  val is the Kotlin variable term
  Item with a capital I calls the constructor. No need for the _New_ keyword
  Item.Settings().group(ItemGroup.MISC)
    This is an argument to the Item constructor
    We are passing it the Item settings of the group MISC
    because of this we will see our item in the MISC section in game.

#### To Register a new item
  Registry.register(Registry.ITEM, Identifier("tomesgeorge", "test_item"), testItem)
  
  Notes:
  Registry is a static variable that keeps track of all items.
  register takes 3 arguments
    1. The registry we are registering to, in this case ITEM
    2. The identifier of the the object. We instantiate this inline.
    3. The object that is being reigstered

#### To port the newly created Fabric + Kotlin mod to actual Minecraft client.
    Once your mod is working, at least at some testable level in the 
    developement environment, you can build the jar file to test in the normal
    Minecraft client.
    
    Prepare your Minecraft Client:
    1. Check gradle.properties, for the minecraft, fabric loader and fabric api 
       versions.
    2. Make sure your Minecraft loader has a profile configured with the
       Correct Minecraft, and fabric loader versions.
    3. In the mods folder, make sure you have the correct fabric api version.
       you will also need:  fabric-language-kotlin-1.3.50+build.3.jar
      ( Or a newer version, So Minecraft can work with your Kotlin coded mod )
    
    Get your mod built and loaded into minecraft client:
    1. in the project folder, type : ./gradlew build
    2. if this is successfull, check the "project folder"/build/lib  folder.
       you should see a file named "project id".jar
    3. Copy this file to your Minecraft Client's mods folder, in the profile 
       you prepared above.



    With this, you should be able to launch your Minecraft client, with your 
    new mod.