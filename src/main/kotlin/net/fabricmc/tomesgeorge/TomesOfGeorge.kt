package net.fabricmc.tomesgeorge

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Items;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

//Trying to create a new item
val testItem = Item(Item.Settings().group(ItemGroup.MISC))
//val id = Identifier("tomesgeorge", "test_item")
@Suppress("unused")
fun init() {
  Registry.register(Registry.ITEM, Identifier("tomesgeorge", "test_item"), testItem)
}

